---

- name: "Set hostname @{{ hostname }}"
  community.general.sysrc:
    name: hostname
    value: "{{ hostname }}"
    state: present
  when: hostname is defined

- name: "Set timezone to {{ timezone }}"
  community.general.timezone:
    name: "{{ timezone }}"
  when: timezone is defined

- name: Tune TTYs
  ansible.builtin.replace:
    path: /etc/ttys
    regexp: '(?:^|\W)secure(?:$|\W)'
    replace: ' insecure'

- name: Configure doas
  ansible.builtin.template:
    src: doas.conf.j2
    dest: /usr/local/etc/doas.conf
    owner: root
    group: wheel
    mode: '0644'
  when: doas_config is defined

- name: "{{ randompid | ternary('Randomize PIDs', 'Do not randomize PIDs') }}"
  ansible.posix.sysctl:
    name: kern.randompid
    state: present
    value: "{{ randompid | ternary('1', '0') }}"
    reload: false

- name: "{{ coredump | ternary('Enable coredump', 'Disable coredump') }}"
  ansible.posix.sysctl:
    name: kern.coredump
    state: present
    value: "{{ coredump | ternary('1', '0') }}"

- name: >-
    {{ unpriv_proc_debug | ternary('Unrestricted debugging',
    'Restricted debugging') }}
  ansible.posix.sysctl:
    name: security.bsd.unprivileged_proc_debug
    state: present
    value: "{{ unpriv_proc_debug | ternary('1', '0') }}"

- name: >-
    "{{ unpriv_read_msgbuf | ternary('Unrestricted message buffer',
    'Restricted message buffer') }}"
  ansible.posix.sysctl:
    name: security.bsd.unprivileged_read_msgbuf
    state: present
    value: "{{ unpriv_read_msgbuf | ternary('1', '0') }}"

- name:
    "Processes running in jails - {{ see_jail_proc | ternary('show', 'hide') }}"
  ansible.posix.sysctl:
    name: security.bsd.see_jail_proc
    state: present
    value: "{{ see_jail_proc | ternary('1', '0') }}"

- name: "{{ see_other_gids | ternary('Show GIDs', 'Hide GIDs') }}"
  ansible.posix.sysctl:
    name: security.bsd.see_other_gids
    state: present
    value: "{{ see_other_gids | ternary('1', '0') }}"

- name: "{{ see_other_uids | ternary('Show UIDs', 'Hide UIDs') }}"
  ansible.posix.sysctl:
    name: security.bsd.see_other_uids
    state: present
    value: "{{ see_other_uids | ternary('1', '0') }}"

- name: "{{ clear_tmp | ternary('Clear /tmp', 'Do not clear /tmp') }}"
  community.general.sysrc:
    name: clear_tmp_enable
    state: present
    value: "{{ clear_tmp | ternary('YES', 'NO') }}"

- name: Enable microcode updates
  community.general.sysrc:
    name: microcode_update_enable
    state: present
    value: "YES"
  notify:
    - Update CPU firmware
  when: microcode_update

- name: "Ntpd | {{ ntpd | ternary('enable', 'disable') }}"
  community.general.sysrc:
    name: ntpd_enable
    state: present
    value: "{{ ntpd | ternary('YES', 'NO') }}"

- name: >-
    {{ ntpd_bootsync | ternary('Sync ntpd on start',
    'Do not sync ntpd on start') }}
  community.general.sysrc:
    name: ntpd_sync_on_start
    state: present
    value: "{{ ntpd_bootsync | ternary('YES', 'NO') }}"

- name: >-
    Ntpd OOM protection | {{ ntpd_oomprotect |
    ternary('activate', 'deactivate') }}
  community.general.sysrc:
    name: ntpd_oomprotect
    state: present
    value: "{{ ntpd_oomprotect | ternary('YES', 'NO') }}"

- name: "Sendmail | incoming - {{ sendmail | ternary('enable', 'disable') }}"
  community.general.sysrc:
    name: sendmail_enable
    state: present
    value: "{{ sendmail | ternary('YES', 'NO') }}"

- name:
    "Sendmail | submit - {{ sendmail_submit | ternary('enable', 'disable') }}"
  community.general.sysrc:
    name: sendmail_submit_enable
    state: present
    value: "{{ sendmail_submit | ternary('YES', 'NO') }}"

- name: >-
    Sendmail | outbound - {{ sendmail_outbound |
    ternary('enable', 'disable') }}
  community.general.sysrc:
    name: sendmail_outbound_enable
    state: present
    value: "{{ sendmail_outbound | ternary('YES', 'NO') }}"

- name: >-
    Sendmail | msp_queue - {{ sendmail_msp_queue |
    ternary('enable', 'disable') }}
  community.general.sysrc:
    name: sendmail_msp_queue_enable
    state: present
    value: "{{ sendmail_msp_queue | ternary('YES', 'NO') }}"

- name: Set autoboot delay
  community.general.sysrc:
    name: autoboot_delay
    state: present
    value: "2"
    path: /boot/loader.conf

- name: "Verbose booting | {{ boot_verbose | ternary('enable', 'disable') }}"
  community.general.sysrc:
    name: boot_verbose
    state: present
    value: "{{ boot_verbose | ternary('YES', 'NO') }}"
    path: /boot/loader.conf

- name: "Change loader logo | {{ loader_logo }}"
  community.general.sysrc:
    name: loader_logo
    state: present
    value: "{{ loader_logo }}"
    path: /boot/loader.conf

- name: "{{ aio | ternary('Enable AIO', 'Disable AIO') }}"
  community.general.sysrc:
    name: aio_load
    state: present
    value: "{{ aio | ternary('YES', 'NO') }}"
    path: /boot/loader.conf

- name: "{{ coretemp | ternary('Enable coretemp', 'Disable coretemp') }}"
  community.general.sysrc:
    name: coretemp_load
    state: present
    value: "{{ coretemp | ternary('YES', 'NO') }}"
    path: /boot/loader.conf

- name: "{{ cpuctl | ternary('Enable cpuctl', 'Disable cpuctl') }}"
  community.general.sysrc:
    name: cpuctl_load
    state: present
    value: "{{ cpuctl | ternary('YES', 'NO') }}"
    path: /boot/loader.conf

- name: "{{ aesni | ternary('Enable AES-NI', 'Disable AES-NI') }}"
  community.general.sysrc:
    name: aesni_load
    state: present
    value: "{{ aesni | ternary('YES', 'NO') }}"
    path: /boot/loader.conf

- name: "{{ cc_htcp | ternary('Enable H-TCP', 'Disable H-TC') }}"
  community.general.sysrc:
    name: cc_htcp_load
    state: present
    value: "{{ cc_htcp | ternary('YES', 'NO') }}"
    path: /boot/loader.conf

- name: "{{ bmcastecho | ternary('Enable ping', 'Disable ping') }}"
  community.general.sysrc:
    name: icmp_bmcastecho
    state: present
    value: "{{ bmcastecho | ternary('YES', 'NO') }}"

- name: >-
    DHCP client background start | {{ dhclient_bg |
    ternary('enable', 'disable') }}
  community.general.sysrc:
    name: background_dhclient
    state: present
    value: "{{ dhclient_bg | ternary('YES', 'NO') }}"

- name: "IPv6 | {{ ipv6_support | ternary('enable', 'disable') }}"
  community.general.sysrc:
    name: rtsold_enable
    state: present
    value: "{{ ipv6_support | ternary('YES', 'NO') }}"

- name: IPv6 | tune
  community.general.sysrc:
    name: rtsold_flags
    state: present
    value: "-aF"
  when: ipv6_support

- name: "Syslogd flags {{ syslogd_flags }}"
  community.general.sysrc:
    name: syslogd_flags
    state: present
    value: "{{ syslogd_flags }}"

- name: "Kernel crash dumps | {{ dumpdev | ternary('enable', 'disable') }}"
  community.general.sysrc:
    name: dumpdev
    state: present
    value: "{{ dumpdev | ternary('YES', 'NO') }}"

- name: Fix file permissions
  ansible.builtin.file:
    path: "{{ item.path }}"
    owner: root
    group: wheel
    mode: "{{ item.mode }}"
  loop:
    - {path: /etc/cron.d, mode: '0700'}
    - {path: /etc/crontab, mode: '0600'}
    - {path: /etc/ssh/sshd_config, mode: '0600'}
